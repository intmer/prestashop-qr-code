{*
* Intmer QR code for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
<div id="intmerqrcode" class="intmerqrcode">
    <img src="{$product_qr_src|escape:'htmlall':'UTF-8'}" width="123" height="123" alt="QR code">
</div>
