<?php
/**
 * Intmer QR code for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

class IntmerQRCode extends Module
{
    const PREFIX = 'INTMER_';

    private $real_path = null;
    private $qr_cache_path = null;
    private $qr_cache_uri = null;

    protected $warnings = array();

    public function __construct()
    {
        $this->name = 'intmerqrcode';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Intmer';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Intmer QR code');
        $this->description = $this->l('This module allows you to add QR code on your product page');

        $this->confirmUninstall = $this->l('Are you sure you want to delete the QR code module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function getWarnings()
    {
        $warnings = (array)$this->warnings;
        $this->warnings = array();

        return $warnings;
    }

    public function getRealPath()
    {
        if (is_null($this->real_path)) {
            $this->real_path = realpath(dirname(__FILE__)).'/';
        }

        return $this->real_path;
    }

    public function getQRCachePath()
    {
        if (is_null($this->qr_cache_path)) {
            $id_shop = (int)Shop::getContextShopID();
            $this->qr_cache_path = _PS_CACHE_DIR_.'qrcode/'.$id_shop.'/';
        }

        return $this->qr_cache_path;
    }

    public function getQRCacheUri()
    {
        if (is_null($this->qr_cache_uri)) {
            $id_shop = (int)Shop::getContextShopID();
            $this->qr_cache_uri = __PS_BASE_URI__.'cache/qrcode/'.$id_shop.'/';
        }

        return $this->qr_cache_uri;
    }

    public function install()
    {
        $result = parent::install();

        $result = $result && require_once(dirname(__FILE__).'/init/install.php');

        $result = $result && $this->registerHook('displayRightColumnProduct');
        // The module need to clear the product page cache after update/delete
        $result = $result && $this->registerHook('actionObjectProductUpdateAfter');
        $result = $result && $this->registerHook('actionObjectProductDeleteAfter');

        return $result;
    }

    private static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir."/".$object)) {
                        self::rrmdir($dir."/".$object);
                    } else {
                        unlink($dir."/".$object);
                    }
                }
            }
            rmdir($dir);
        }
    }

    public function uninstall()
    {
        require_once(dirname(__FILE__).'/init/uninstall.php');

        self::rrmdir(_PS_CACHE_DIR_.'qrcode/');

        return parent::uninstall();
    }

    protected function clearProductCache($product_id)
    {
        $cache_path = $this->getQRCachePath();
        $cache_file_name = 'qr_'.$product_id.'.png';
        $cache_file = $cache_path.$cache_file_name;
        if (file_exists($cache_file)) {
            unlink($cache_file);
        }

        return $this->_clearCache('index.tpl', $this->name.'|'.$product_id);
    }

    public function actionFrontEnd($params)
    {
        $params;

        require_once dirname(__FILE__).'/classes/mobile_detect/mobile_detect.php';
        $detect = new MobileDetect;

        if ($detect->isMobile()) {
            return '';
        }

        if (!isset($this->context->controller) || !method_exists($this->context->controller, 'getProduct')) {
            return '';
        }

        $product = $this->context->controller->getProduct();

        if (!(isset($product->id) && $product->id)) {
            return '';
        }

        $product_id = (int)$product->id;

        if (!$this->isCached('index.tpl', $this->getCacheId($this->name.'|'.$product_id))) {
            $cache_path = $this->getQRCachePath();
            $cache_file_name = 'qr_'.$product_id.'.png';
            $cache_file = $cache_path.$cache_file_name;
            if (!file_exists($cache_path)) {
                $path_created = mkdir($cache_path, 0777, true);
                if (!$path_created) {
                    $this->warnings[] = $this->l('Cache folder is not writable');
                }
            }
            if (file_exists($cache_file)) {
                unlink($cache_file);
            }
            require_once dirname(__FILE__).'/classes/phpqrcode/phpqrcode.php';
            QRcode::png($product->getLink(), $cache_file);

            $this->context->smarty->assign(array(
                'product_id' => $product_id,
                'product_qr_src' => $this->getQRCacheUri().$cache_file_name,
                'product' => $product,
            ));
        }

        return $this->display(__FILE__, 'index.tpl', $this->getCacheId($this->name.'|'.$product_id));
    }

    public function hookDisplayRightColumnProduct($params)
    {
        return $this->actionFrontEnd($params);
    }

    public function hookActionObjectProductUpdateAfter($params)
    {
        return $this->clearProductCache($params['object']->id);
    }

    public function hookActionObjectProductDeleteAfter($params)
    {
        return $this->clearProductCache($params['object']->id);
    }
}
